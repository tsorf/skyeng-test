package ru.frosteye.ovsa.domain.network.base

class ApiException(
    message: String?,
    val code: Int
) : RuntimeException(message) {

}
