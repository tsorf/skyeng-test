package ru.frosteye.ovsa.presentation.view.contract

import ru.frosteye.ovsa.presentation.adapter.ModelViewHolder
import kotlin.reflect.KProperty


public fun <T : Any> setter(
    set: (T) -> Unit
): LateInit<T> = LateInitImpl({ value }, {
    value = it
    set(value)
})

interface InteractiveModelView<T> : ModelView<T> {
    var listener: Listener?

    interface Listener {
        fun onModelAction(code: Int, payload: Any? = null)
    }
}

interface Positioned {

    var position: Int

    data class Holder(
        val position: Int,
        val payload: Any? = null
    ) {

        fun <T>payload(): T? {
            return payload as T?
        }
    }
}

class LateInitImpl<T : Any>(
    private val getter: FieldHolderImpl<T>.() -> T = { value },
    private val setter: FieldHolderImpl<T>.(T) -> Unit = { value = it }
) : LateInit<T> {
    private val fieldHolder = FieldHolderImpl<T>()

    override fun getValue(thisRef: Any?, property: KProperty<*>) = fieldHolder.getter()
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) =
        fieldHolder.setter(value)

    class FieldHolderImpl<T : Any> : LateInit.FieldHolder<T> {
        override lateinit var value: T
    }
}

interface ViewHolderChild : Positioned {

    var viewHolder: ModelViewHolder<*>?
}