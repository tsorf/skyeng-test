package ru.frosteye.ovsa.app.di.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.PowerManager;
import android.view.WindowManager;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.internal.functions.Functions;
import io.reactivex.plugins.RxJavaPlugins;
import ru.frosteye.ovsa.domain.executor.MainThread;
import ru.frosteye.ovsa.domain.executor.ThreadExecutor;
import ru.frosteye.ovsa.domain.executor.UIThread;

@Module
public abstract class BaseAppModule<T extends Context> {

    protected T context;

    public BaseAppModule(T context) {
        this.context = context;
        RxJavaPlugins.setErrorHandler(Functions.<Throwable>emptyConsumer());
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return context;
    }

    @Provides @Singleton
    public MainThread provideMainThread() {
        return new UIThread();
    }

    @Provides @Singleton
    public Executor provideExecutor(ThreadExecutor executor) {
        return executor;
    }

    @Provides @Singleton
    public PowerManager providePowerManager() {
        return ((PowerManager) context.getSystemService(Context.POWER_SERVICE));
    }

    @Provides @Singleton
    public AudioManager provideAudioManager() {
        return ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE));
    }

    @Provides @Singleton
    public LocationManager provideLocationManager() {
        return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE));
    }

    @Provides @Singleton
    public WindowManager provideWindowManager() {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
    }

    @Provides @Singleton
    public SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(provideDefaultPreferencesName(), Context.MODE_PRIVATE);
    }

    protected String provideDefaultPreferencesName() {
        return context.getPackageName();
    };
}
