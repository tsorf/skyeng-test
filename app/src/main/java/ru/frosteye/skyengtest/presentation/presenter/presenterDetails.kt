package ru.frosteye.skyengtest.presentation.presenter

import android.content.Intent
import ru.frosteye.ovsa.presentation.presenter.BasePresenter
import ru.frosteye.ovsa.presentation.presenter.LivePresenter
import ru.frosteye.skyengtest.data.model.Word
import ru.frosteye.skyengtest.presentation.view.contract.MeaningsView
import java.lang.IllegalArgumentException
import javax.inject.Inject

interface MeaningsPresenter : LivePresenter<MeaningsView> {

}

class MeaningsPresenterImpl @Inject constructor(

) : BasePresenter<MeaningsView>(), MeaningsPresenter {

    private lateinit var word: Word

    override fun onSetIntent(intent: Intent) {
        super.onSetIntent(intent)
        word = getIntentPayload()
            ?: throw IllegalArgumentException("no word")
    }

    override fun onAttach(v: MeaningsView, vararg params: Any) {
        super.onAttach(v, *params)
        view.showWord(word)
    }
}