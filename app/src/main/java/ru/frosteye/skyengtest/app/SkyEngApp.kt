package ru.frosteye.skyengtest.app

import android.app.Application
import ru.frosteye.ovsa.domain.network.base.ApiException
import ru.frosteye.ovsa.domain.task.ApiExceptionInterloper
import ru.frosteye.skyengtest.app.di.AppComponent
import ru.frosteye.skyengtest.app.di.AppModule
import ru.frosteye.skyengtest.app.di.DaggerAppComponent

class SkyEngApp : Application(), ApiExceptionInterloper.Listener  {


    override fun onCreate() {
        super.onCreate()

        ApiExceptionInterloper.addListener(this)
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onApiException(e: ApiException) {

    }

    companion object {

        lateinit var appComponent: AppComponent
    }
}