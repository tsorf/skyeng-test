package ru.frosteye.ovsa.domain.network.client

interface IdentityProvider {
    fun provideIdentity(): String?
}
