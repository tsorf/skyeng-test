package ru.frosteye.skyengtest.data.model

import ru.frosteye.ovsa.presentation.adapter.AdapterItem
import ru.frosteye.skyengtest.R
import ru.frosteye.skyengtest.presentation.view.impl.item.MeaningItemView
import ru.frosteye.skyengtest.presentation.view.impl.item.WordItemView

class WordAdapterItem(model: Word) : AdapterItem<Word, WordItemView>(model) {

    override val layoutResource: Int = R.layout.view_item_word
}

class MeaningAdapterItem(model: Meaning) : AdapterItem<Meaning, MeaningItemView>(model) {

    override val layoutResource: Int = R.layout.view_item_meaning
}