package ru.frosteye.ovsa.presentation.view.impl.activity

import android.content.Intent
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import ru.frosteye.ovsa.presentation.presenter.LivePresenter
import ru.frosteye.ovsa.presentation.view.contract.BasicView
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView


abstract class PresenterActivity : OvsaActivity(), BasicView, InteractiveModelView.Listener {


    private var savedInstanceState: Bundle? = null
    var isEnabled: Boolean = true
        private set

    protected abstract val presenter: LivePresenter<*>?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.savedInstanceState = savedInstanceState
    }

    protected open fun inject(component: Any) {
        try {
            component.javaClass
                    .getMethod("inject", javaClass)
                    .invoke(component, this)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    override fun onModelAction(code: Int, payload: Any?) {

    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        initView(savedInstanceState)
        presenter?.onSetIntent(intent)
        attachPresenter()
    }

    @CallSuper
    override fun enableControls(enabled: Boolean, code: Int) {
        this.isEnabled = enabled
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter?.onActivityResult(requestCode, resultCode, data)
    }

    protected open fun initView(savedInstanceState: Bundle?) {

    }

    protected abstract fun attachPresenter()
}
