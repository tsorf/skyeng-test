package ru.frosteye.skyengtest.app.di

import android.content.Context
import dagger.Component
import dagger.Subcomponent
import ru.frosteye.ovsa.di.PresenterScope
import ru.frosteye.ovsa.presentation.view.impl.activity.MeaningsActivity
import ru.frosteye.skyengtest.presentation.view.impl.activity.BaseActivity
import ru.frosteye.skyengtest.presentation.view.impl.activity.MainActivity
import javax.inject.Singleton

@Component(modules = [AppModule::class, RepoModule::class, ApiModule::class])
@Singleton
interface AppComponent {

    operator fun plus(module: PresenterModule): PresenterComponent

    fun context(): Context
}

@Subcomponent(modules = [PresenterModule::class])
@PresenterScope
interface PresenterComponent {

    fun inject(activity: BaseActivity)
    fun inject(activity: MainActivity)
    fun inject(activity: MeaningsActivity)

//    fun inject(fragment: BaseFragment)
}
