package ru.frosteye.skyengtest.presentation.view.impl.item

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import kotlinx.android.synthetic.main.view_item_word.view.*
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView
import ru.frosteye.ovsa.presentation.view.contract.setter
import ru.frosteye.ovsa.presentation.view.impl.view.BaseLinearLayout
import ru.frosteye.ovsa.presentation.view.impl.view.dpToPx
import ru.frosteye.ovsa.presentation.view.impl.view.drawBottomLine
import ru.frosteye.skyengtest.R
import ru.frosteye.skyengtest.app.environment.ACTION_SELECT_WORD
import ru.frosteye.skyengtest.data.model.Word

class WordItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BaseLinearLayout(context, attrs, defStyleAttr), InteractiveModelView<Word> {


    override var listener: InteractiveModelView.Listener? = null
    private var dividerLeftPadding: Float = 0f
    private lateinit var dividerPaint: Paint
    override var model: Word by setter {
        wordText.text = it.text
        val meaningsCount = it.meanings.size
        wordMeaningsCount.text = resources.getQuantityString(R.plurals.meanings, meaningsCount, meaningsCount)
    }

    override fun prepareView() {
        setWillNotDraw(false)
        dividerPaint = Paint().apply {
            color = resources.getColor(R.color.blackSemiLight)
            strokeWidth = dpToPx(0.5f)
        }
        dividerLeftPadding = paddingStart.toFloat()
        setOnClickListener {
            listener?.onModelAction(ACTION_SELECT_WORD, model)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawBottomLine(canvas, dividerPaint, dividerLeftPadding)
    }
}