package ru.frosteye.skyengtest.domain.network

import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.Interceptor
import retrofit2.HttpException
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.frosteye.ovsa.domain.network.base.ApiException
import ru.frosteye.ovsa.domain.network.client.DistributedRetrofitClient
import ru.frosteye.ovsa.domain.network.client.IdentityProvider
import ru.frosteye.ovsa.domain.network.client.RestApi
import ru.frosteye.ovsa.domain.serialization.DateSerializer
import ru.frosteye.skyengtest.BuildConfig
import ru.frosteye.skyengtest.data.model.Word
import java.util.*
import javax.inject.Inject


@RestApi
interface TranslateApi {

    @GET("words/search")
    fun findWords(
        @Query("search") search: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int = 5
    ): Single<List<Word>>

}



class RestClient @Inject constructor(

) : DistributedRetrofitClient(
    setOf(
        TranslateApi::class.java
    )
) {

    override fun url(forClass: Class<out Any>): String {
        return BuildConfig.BASE_URL
    }

    override fun createGsonBuilder(forClass: Class<out Any>, original: GsonBuilder): GsonBuilder {
        return super.createGsonBuilder(forClass, original).apply {
            registerTypeAdapter(Date::class.java, DateSerializer(false))
        }
    }

    override fun identity(forClass: Class<out Any>): IdentityProvider? {
        return null
    }

    override fun createClientInterceptors(
        forClass: Class<out Any>,
        original: MutableList<Interceptor>
    ): MutableList<Interceptor> {
        return super.createClientInterceptors(forClass, original).apply {
            add(Interceptor {
                val builder = it.request().newBuilder()
//                builder.addHeader("X-Requesting-Device", ResHelper.deviceInfoString)
                it.proceed(builder.build())
            })
        }
    }
}



fun Throwable.isLimitException(): Boolean {
    return this is ApiException && code == 429
            || this is HttpException && this.code() == 429
}

fun Throwable.isNotFoundException(): Boolean {
    return this is ApiException && code == 404
            || this is HttpException && this.code() == 404
}

fun Throwable.isBadRequestException(): Boolean {
    return this is ApiException && code == 400
            || this is HttpException && this.code() == 400
}