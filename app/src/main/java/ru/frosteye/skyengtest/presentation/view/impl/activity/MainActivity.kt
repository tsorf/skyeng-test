package ru.frosteye.skyengtest.presentation.view.impl.activity

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import ru.frosteye.ovsa.domain.misc.cast
import ru.frosteye.ovsa.domain.misc.startActivity
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter
import ru.frosteye.ovsa.presentation.view.impl.activity.MeaningsActivity
import ru.frosteye.ovsa.presentation.view.impl.stub.EndlessRecyclerOnScrollListener
import ru.frosteye.ovsa.presentation.view.impl.view.trimmedOrNull
import ru.frosteye.skyengtest.R
import ru.frosteye.skyengtest.app.environment.ACTION_SELECT_WORD
import ru.frosteye.skyengtest.data.model.Word
import ru.frosteye.skyengtest.data.model.WordAdapterItem
import ru.frosteye.skyengtest.presentation.presenter.MainPresenter
import ru.frosteye.skyengtest.presentation.view.contract.MainView
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {

    @Inject
    override lateinit var presenter: MainPresenter
    private lateinit var adapter: FlexibleModelAdapter<WordAdapterItem>
    private lateinit var onScrollListener: EndlessRecyclerOnScrollListener
    private var page = 1
    private var query: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        adapter = FlexibleModelAdapter(this)
        val layoutManager = LinearLayoutManager(this)
        onScrollListener = object : EndlessRecyclerOnScrollListener(layoutManager) {

            override fun onLoadMore(currentPage: Int) {
                page = currentPage
                findWords()
            }
        }
        mainList.let {
            it.layoutManager = layoutManager
            it.adapter = adapter
            it.addOnScrollListener(onScrollListener)
        }
        mainSwipe.setOnRefreshListener {
            findWords()
        }
        mainSearchInput.doAfterTextChanged { _ ->
            mainSearchInput.trimmedOrNull()?.let {
                if (it.isNotBlank()) {
                    query = it
                    reset()
                    findWords()
                } else {
                    query = ""
                    reset()
                }
            }
        }
        mainClear.setOnClickListener {
            mainSearchInput.text = null
            reset()
        }
    }

    override fun onModelAction(code: Int, payload: Any?) {
        when(code) {
            ACTION_SELECT_WORD -> {
                payload.cast<Word>()?.let {
                    startActivity(MeaningsActivity::class.java, it)
                }
            }
        }
    }

    private fun findWords() {
        presenter.onSearch(query, page)
    }

    override fun attachPresenter() {
        presenter.onAttach(this)
    }

    override fun appendItems(items: List<Word>) {
        adapter.addItems(adapter.itemCount, items.map { WordAdapterItem(it) })
    }

    override fun enableControls(enabled: Boolean, code: Int) {
        super.enableControls(enabled, code)
        mainProgress.isVisible = !enabled
        if (enabled) mainSwipe.post {
            mainSwipe.isRefreshing = false
        }
    }


    private fun reset() {
        adapter.clear()
        onScrollListener.reset()
        page = 1
    }

    override fun showMessage(message: CharSequence?, code: Int) {

    }
}