package ru.frosteye.ovsa.presentation.view.impl.activity

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_meanings.*
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter
import ru.frosteye.skyengtest.R
import ru.frosteye.skyengtest.data.model.MeaningAdapterItem
import ru.frosteye.skyengtest.data.model.Word
import ru.frosteye.skyengtest.presentation.presenter.MeaningsPresenter
import ru.frosteye.skyengtest.presentation.view.contract.MeaningsView
import ru.frosteye.skyengtest.presentation.view.impl.activity.BaseActivity
import javax.inject.Inject

class MeaningsActivity : BaseActivity(), MeaningsView {

    @Inject
    override lateinit var presenter: MeaningsPresenter
    private lateinit var adapter: FlexibleModelAdapter<MeaningAdapterItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meanings)
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(meaningsToolbar)
        enableBackButton()

        adapter = FlexibleModelAdapter(this)
        meaningsList.adapter = adapter
    }

    override fun attachPresenter() {
        presenter.onAttach(this)
    }

    override fun showWord(word: Word) {
        meaningsCollapsingToolbar.title = word.text.capitalize()
        adapter.updateDataSet(word.meanings.map { MeaningAdapterItem(it) })
    }
}