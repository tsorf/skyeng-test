package ru.frosteye.skyengtest.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.lang.StringBuilder

@Parcelize
data class Word(
    @SerializedName("id") val id: Long,
    @SerializedName("text") val text: String,
    @SerializedName("meanings") val meanings: List<Meaning>
): Parcelable

@Parcelize
data class Meaning(
    @SerializedName("id") val id: Long,
    @SerializedName("partOfSpeechCode") val partOfSpeechCode: String,
    @SerializedName("translation") val translation: Translation,
    @SerializedName("previewUrl") val previewUrl: String,
    @SerializedName("imageUrl") val imageUrl: String,
    @SerializedName("transcription") val transcription: String
): Parcelable

@Parcelize
data class Translation(
    @SerializedName("text") val text: String,
    @SerializedName("note") val note: String?
): Parcelable {

    fun format(): CharSequence {
        return StringBuilder().apply {
            append(text)
            note?.let { append(it) }
        }
    }
}