package ru.frosteye.skyengtest.presentation.presenter

import io.reactivex.disposables.Disposable
import ru.frosteye.ovsa.domain.task.request
import ru.frosteye.ovsa.presentation.presenter.BasePresenter
import ru.frosteye.ovsa.presentation.presenter.LivePresenter
import ru.frosteye.skyengtest.data.repo.TranslateRepo
import ru.frosteye.skyengtest.presentation.view.contract.MainView
import javax.inject.Inject

interface MainPresenter : LivePresenter<MainView> {

    fun onSearch(query: String, page: Int)
}

class MainPresenterImpl @Inject constructor(
    private val translateRepo: TranslateRepo
) : BasePresenter<MainView>(), MainPresenter {

    private var lastCall: Disposable? = null

    override fun onSearch(query: String, page: Int) {
        enableControls(false)

        lastCall?.dispose()
        lastCall = translateRepo.findWords(query, page)
//        +translateRepo.findWords(query, page)
            .request({
                enableControls(true)
                view.appendItems(it)
            }, {
                showError(it.message)
                enableControls(true)
            })
    }
}