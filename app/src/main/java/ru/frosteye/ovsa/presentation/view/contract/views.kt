package ru.frosteye.ovsa.presentation.view.contract

import kotlin.properties.ReadWriteProperty

interface BasicView {

    fun enableControls(enabled: Boolean, code: Int = 0)
    fun showMessage(message: CharSequence?, code: Int)

}

interface ModelView<T> {
    var model: T
}

interface LateInit<T : Any> : ReadWriteProperty<Any?, T> {

    interface FieldHolder<T> {
        var value: T
    }
}