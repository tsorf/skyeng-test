package ru.frosteye.ovsa.presentation.view.impl.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.drawable.Animatable
import android.net.Uri
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import ru.frosteye.skyengtest.R
import java.util.*


abstract class OvsaActivity : AppCompatActivity() {
    private var backButtonEnabled = false
    private var topLoadingShown = false

    val loaderDrawable: AnimatedVectorDrawableCompat?
        get() {
            try {

                return AnimatedVectorDrawableCompat.create(this, R.drawable.ovsa_menu_loader_animated)
            } catch (e: Exception) {
                return null
            }

        }


    protected fun applyBackArrowColor(@ColorRes color: Int) {
        try {
            ContextCompat.getDrawable(this, R.drawable.ic_back)?.apply {
                setColorFilter(ContextCompat.getColor(this@OvsaActivity, color), PorterDuff.Mode.SRC_ATOP)
                supportActionBar?.setHomeAsUpIndicator(this)
            }


        } catch (ignored: Exception) {
        }

    }

    protected open fun enableBackButton() {
        try {
            supportActionBar?.let {
                it.setDisplayHomeAsUpEnabled(true)
                backButtonEnabled = true
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    protected fun <T> getSerializable(key: String, clazz: Class<T>): T? {
        if (intent.extras == null) return null
        val serializable = intent.getSerializableExtra(key)
        return if (serializable != null) {
            try {
                clazz.cast(serializable)
            } catch (e: Exception) {
                null
            }

        } else null
    }

    protected fun setOnDoneListener(textView: TextView, onDoneListener: () -> Unit) {
        textView.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                onDoneListener.invoke()
            }
            false
        }
    }

    open fun showTopBarLoading(shown: Boolean) {
        this.topLoadingShown = shown
        invalidateOptionsMenu()
    }

    protected fun openUrl(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    protected fun dialPhone(phone: String) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone"))
        startActivity(intent)
    }

    protected fun composeEmail(to: String, description: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent.data = Uri.parse(to)
        startActivity(Intent.createChooser(emailIntent, description))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        try {
            val icon = AnimatedVectorDrawableCompat.create(this, R.drawable.ovsa_menu_loader_animated)
            menu.add(0, MENU_LOADING, 10, "Loading")
                    .setIcon(icon)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        } catch (ignored: Exception) {
        }

        return super.onCreateOptionsMenu(menu)
    }

    fun startActivity(clazz: Class<out Activity>, finish: Boolean) {
        val intent = Intent(this, clazz)
        startActivity(intent)
        if (finish) finish()
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        processTopLoading(menu)
        return super.onPrepareOptionsMenu(menu)
    }

    private fun processTopLoading(menu: Menu) {
        try {
            val menuItemLoader = menu.findItem(MENU_LOADING)
            val menuItemLoaderIcon = menuItemLoader.icon
            if (menuItemLoaderIcon != null) {
                if (topLoadingShown) {
                    menuItemLoader.isVisible = true
                    (menuItemLoader.icon as Animatable).start()
                } else {
                    menuItemLoader.isVisible = false
                }
            }
        } catch (ignored: Exception) {

        }

    }

    protected fun showKeyboard(editText: EditText) {
        Handler().postDelayed({
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }, 500)
    }




    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (!backButtonEnabled) return true
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    open fun showError(error: String) {
        showToast(error)
    }

    open fun showToast(message: CharSequence) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    open fun showSuccess(message: String) {
        showToast(message)
    }


    open fun showMessage(message: CharSequence?, code: Int) {
        when (code) {
            -1 -> showError(message.toString())
            0 -> showToast(message.toString())
            else -> showSuccess(message.toString())
        }
    }

    companion object {
        val TAG = OvsaActivity::class.java.simpleName
        private val MENU_LOADING = 123
    }
}
