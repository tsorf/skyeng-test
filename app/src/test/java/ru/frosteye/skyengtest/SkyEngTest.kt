package ru.frosteye.skyengtest

import org.junit.Test

import org.junit.Assert.*
import ru.frosteye.skyengtest.app.MonsterClass
import java.lang.IllegalStateException

class SkyEngTest {

    @Test
    fun checkMonster() {
        val monster = MonsterClass()
        monster.booo()
        assertEquals(monster.hasBoooed, true)
    }

    @Test(expected = IllegalStateException::class)
    fun checkMonsterFail() {
        val monster = MonsterClass()
        monster.failBoo()
    }
}