package ru.frosteye.ovsa.domain.misc

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Parcelable


inline fun <reified T> Any?.cast(): T? {
    if (this == null) return null
    return if (this is T) {
        this
    } else null
}

fun Context.startActivity(clazz: Class<out Activity>, vararg payloads: Parcelable?) {
    val intent = Intent(this, clazz)
    payloads.forEachIndexed { i, p ->
        p?.let {
            val name = p::class.java.name
            intent.putExtra(name, p)
        }
    }
    startActivity(intent)
}