package ru.frosteye.ovsa.presentation.view.impl.view

import android.graphics.Canvas
import android.graphics.Paint
import android.util.TypedValue
import android.view.View


fun View.drawBottomLine(canvas: Canvas, paint: Paint, left: Float = 0f) {
    canvas.drawLine(
        left, measuredHeight.toFloat(),
        measuredWidth.toFloat(), measuredHeight.toFloat(),
        paint
    )
}

fun View.dpToPx(value: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        value,
        resources.displayMetrics
    )
}