package ru.frosteye.skyengtest.data.repo

import io.reactivex.Observable
import ru.frosteye.ovsa.domain.task.toBack
import ru.frosteye.skyengtest.data.model.Word
import ru.frosteye.skyengtest.domain.network.TranslateApi
import javax.inject.Inject

interface TranslateRepo {

    fun findWords(query: String, page: Int): Observable<List<Word>>
}

class TranslateRepoImpl @Inject constructor(
    private val translateApi: TranslateApi
) : TranslateRepo {

    override fun findWords(query: String, page: Int): Observable<List<Word>> {
        return translateApi.findWords(query, page).toBack().toObservable()
    }
}