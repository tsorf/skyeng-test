import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadImage(
    url: String?, @DrawableRes default: Int,
    requestOptions: RequestOptions
) {
    if (url == null) {
        Glide.with(context)
            .load(default)
            .apply(requestOptions)
            .into(this)
    } else {
        Glide.with(context)
            .load(url)
            .apply(requestOptions)
            .into(this)
    }
}