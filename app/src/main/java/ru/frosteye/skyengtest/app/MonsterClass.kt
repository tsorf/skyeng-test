package ru.frosteye.skyengtest.app

import java.lang.IllegalStateException

class MonsterClass {

    var hasBoooed = false
        private set

    fun booo() {
        print("Boooo!!!")
        hasBoooed = true
    }

    fun failBoo() {
        throw IllegalStateException("can't booo")
    }
}