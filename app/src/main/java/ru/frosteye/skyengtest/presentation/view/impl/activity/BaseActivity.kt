package ru.frosteye.skyengtest.presentation.view.impl.activity

import android.os.Bundle
import ru.frosteye.ovsa.presentation.view.impl.activity.PresenterActivity
import ru.frosteye.skyengtest.app.SkyEngApp
import ru.frosteye.skyengtest.app.di.PresenterModule

abstract class BaseActivity : PresenterActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val component = SkyEngApp.appComponent + PresenterModule(this)
        component.inject(this)
        inject(component)

    }
}