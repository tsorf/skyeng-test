package ru.frosteye.ovsa.presentation.view.impl.view

import android.content.Context
import java.lang.reflect.Field

object UITools {


    fun getResourceDeclareStyleableIntArray(context: Context, name: String): IntArray? {
        fun fields(packageName: String): Array<Field>? = try {
            Class.forName("$packageName.R\$styleable").fields
        } catch (e: Exception) {
            null
        }

        val array = fields(context.packageName) ?: fields(context.packageName.replace(".dev", ""))
        array?.let {
            try {
                for (f in it) {
                    if (f.name == name) {
                        return f.get(null) as IntArray
                    }
                }
            } catch (t: Throwable) {
            }
        }

        return null
    }
}