package ru.frosteye.ovsa.presentation.view.impl.view

import android.widget.TextView


fun TextView.trimmedOrNull() : String? {
    val value = text.toString().trim()
    return if (value.isEmpty()) null else value
}