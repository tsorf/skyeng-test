package ru.frosteye.skyengtest.presentation.view.impl.item

import android.content.Context
import android.util.AttributeSet
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.view_item_meaning.view.*
import loadImage
import ru.frosteye.ovsa.presentation.view.contract.InteractiveModelView
import ru.frosteye.ovsa.presentation.view.contract.setter
import ru.frosteye.ovsa.presentation.view.impl.view.BaseLinearLayout
import ru.frosteye.ovsa.presentation.view.impl.view.dpToPx
import ru.frosteye.skyengtest.R
import ru.frosteye.skyengtest.data.model.Meaning

class MeaningItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BaseLinearLayout(context, attrs, defStyleAttr), InteractiveModelView<Meaning> {

    override var listener: InteractiveModelView.Listener? = null
    override var model: Meaning by setter {
        meaningImage.loadImage(
            "https:${it.previewUrl}",
            R.drawable.ic_image_placeholder,
            RequestOptions().transform(
                CenterCrop(),
                RoundedCorners(dpToPx(4f).toInt())
            )
        )
        meaningTranscription.text =
            resources.getString(R.string.template_transcription, it.transcription)
        meaningTranslation.text = it.translation.format()
    }

    override fun prepareView() {

    }
}