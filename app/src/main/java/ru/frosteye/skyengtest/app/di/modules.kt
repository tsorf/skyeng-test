package ru.frosteye.skyengtest.app.di

import android.view.View
import androidx.fragment.app.Fragment
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import ru.frosteye.ovsa.app.di.module.BaseAppModule
import ru.frosteye.ovsa.app.di.module.BasePresenterModule
import ru.frosteye.ovsa.di.PresenterScope
import ru.frosteye.ovsa.domain.network.client.ApiClient
import ru.frosteye.ovsa.domain.serialization.DateSerializer
import ru.frosteye.ovsa.domain.serialization.GsonSerializer
import ru.frosteye.ovsa.domain.serialization.Serializer
import ru.frosteye.skyengtest.app.SkyEngApp
import ru.frosteye.skyengtest.data.repo.TranslateRepo
import ru.frosteye.skyengtest.data.repo.TranslateRepoImpl
import ru.frosteye.skyengtest.domain.network.*
import ru.frosteye.skyengtest.presentation.presenter.MeaningsPresenter
import ru.frosteye.skyengtest.presentation.presenter.MeaningsPresenterImpl
import ru.frosteye.skyengtest.presentation.presenter.MainPresenter
import ru.frosteye.skyengtest.presentation.presenter.MainPresenterImpl
import ru.frosteye.skyengtest.presentation.view.impl.activity.BaseActivity
import java.util.*
import javax.inject.Singleton

@Module
class AppModule(context: SkyEngApp) : BaseAppModule<SkyEngApp>(context) {



    @Provides
    @Singleton
    fun apiClient(impl: RestClient): ApiClient = impl

    @Provides
    @Singleton
    fun serializer(): Serializer {
        return GsonSerializer(GsonBuilder()
            .registerTypeAdapter(Date::class.java, DateSerializer(false))
            .create()
        )
    }


}

@Module
class PresenterModule : BasePresenterModule<BaseActivity, Fragment> {

    constructor(view: View) : super(view)
    constructor(activity: BaseActivity) : super(activity)
    constructor(fragment: Fragment) : super(fragment)
    constructor() : super()

    @Provides @PresenterScope fun main(impl: MainPresenterImpl): MainPresenter = impl
    @Provides @PresenterScope fun meanings(impl: MeaningsPresenterImpl): MeaningsPresenter = impl
}

@Module
class RepoModule {

    @Provides @Singleton fun translate(impl: TranslateRepoImpl): TranslateRepo = impl
}

@Module
class ApiModule {

    @Provides @Singleton fun translateApi(client: ApiClient): TranslateApi = client.apiFor(TranslateApi::class.java)
}
