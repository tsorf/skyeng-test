package ru.frosteye.ovsa.domain.executor

import io.reactivex.Scheduler

interface MainThread {
    val scheduler: Scheduler
}
