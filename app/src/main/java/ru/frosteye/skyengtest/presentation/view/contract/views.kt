package ru.frosteye.skyengtest.presentation.view.contract

import ru.frosteye.ovsa.presentation.view.contract.BasicView
import ru.frosteye.skyengtest.data.model.Word

interface MainView : BasicView {

    fun appendItems(items: List<Word>)
}

interface MeaningsView : BasicView {

    fun showWord(word: Word)
}